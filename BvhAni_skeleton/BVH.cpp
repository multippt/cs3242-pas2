/** 
**/
#define _USE_MATH_DEFINES

#include <fstream>
#include <sstream>
#include <iostream> //debuging
#include "BVH.h"

BVH::BVH()
{
	load_success=false;
}

BVH::BVH(const char* filename)
{
	std::string filenamestr(filename);
	load_success=false;
	load(filename);
}

BVH::~BVH()
{
	clear();
}

void BVH::clear()
{
	nameToJoint.clear();
	jointList.clear();
	channels.clear();
	load_success=false;
	rootJoint=NULL;
}

void  BVH::load(const std::string& bvh_file_name)
{
	#define BUFFER_LENGTH 1024*4
	std::ifstream file;
	char line[BUFFER_LENGTH];
	char* token;
	char separater[] = " :,\t";
	std::vector<JOINT*> joint_stack;
	JOINT* joint = NULL;
	JOINT* new_joint = NULL;
	bool is_site = false;
	double x, y ,z;	
	clear();		

	file.open(bvh_file_name, std::ios::in);
	if (!file.is_open())  return; 

	while (!file.eof())
	{
		if (file.eof())  goto bvh_error;

		file.getline( line, BUFFER_LENGTH );
		token = strtok( line, separater );

		if ( token == NULL )  continue;

		if ( strcmp( token, "{" ) == 0 )
		{
			joint_stack.push_back( joint );
			joint = new_joint;
			continue;
		}
		if ( strcmp( token, "}" ) == 0 )
		{
			joint = joint_stack.back();
			joint_stack.pop_back();
			is_site = false;
			continue;
		}

		if ( ( strcmp( token, "ROOT" ) == 0 ) ||
		     ( strcmp( token, "JOINT" ) == 0 ) )
		{
			new_joint = new JOINT();			
			new_joint->parent = joint;
			new_joint->is_site = false;
			new_joint->offset.x = 0.0;  new_joint->offset.y = 0.0;  new_joint->offset.z = 0.0;	
			//add to joint collection
			jointList.push_back( new_joint );
			//add children
			if ( joint )
				joint->children.push_back( new_joint );
			else
				rootJoint = new_joint; //the root
			token = strtok( NULL, "" );
			while ( *token == ' ' )  token ++;
			new_joint->name = std::string(token);
			nameToJoint[ new_joint->name ] = new_joint;
			continue;
		}

		if ( ( strcmp( token, "End" ) == 0 ) )
		{			
			new_joint = new JOINT();			
			new_joint->parent = joint;			
			new_joint->name = "EndSite";
			new_joint->is_site = true;
			new_joint->channels.clear();
			//add children 
			if ( joint )
				joint->children.push_back( new_joint );
			else
				rootJoint = new_joint; //can an endsite be root? -cuizh
			//add to joint collection
			jointList.push_back( new_joint );
			is_site = true;			
			continue;
		}

		if ( strcmp( token, "OFFSET" ) == 0 )
		{
			token = strtok( NULL, separater );
			x = token ? atof( token ) : 0.0;
			token = strtok( NULL, separater );
			y = token ? atof( token ) : 0.0;
			token = strtok( NULL, separater );
			z = token ? atof( token ) : 0.0;			
			joint->offset.x = x;
			joint->offset.y = y;
			joint->offset.z = z;
			continue;
		}

		if ( strcmp( token, "CHANNELS" ) == 0 )
		{
			token = strtok( NULL, separater );
			joint->channels.resize( token ? atoi( token ) : 0 );

			for ( uint i=0; i<joint->channels.size(); i++ )
			{
				CHANNEL* channel = new CHANNEL();
				channel->index = channels.size();
				channels.push_back(channel);
				joint->channels[i] = channel;

				token = strtok( NULL, separater );
				if ( strcmp( token, "Xrotation" ) == 0 )
					channel->type = X_ROTATION;
				else if ( strcmp( token, "Yrotation" ) == 0 )
					channel->type = Y_ROTATION;
				else if ( strcmp( token, "Zrotation" ) == 0 )
					channel->type = Z_ROTATION;
				else if ( strcmp( token, "Xposition" ) == 0 )
					channel->type = X_POSITION;
				else if ( strcmp( token, "Yposition" ) == 0 )
					channel->type = Y_POSITION;
				else if ( strcmp( token, "Zposition" ) == 0 )
					channel->type = Z_POSITION;
			}			
		}

		if ( strcmp( token, "MOTION" ) == 0 )
			break;
	}

	file.getline( line, BUFFER_LENGTH );
	token = strtok( line, separater );
	if ( strcmp( token, "Frames" ) != 0 )  goto bvh_error;
	token = strtok( NULL, separater );
	if ( token == NULL )  goto bvh_error;
	motionData.num_frames = atoi( token );

	file.getline( line, BUFFER_LENGTH );
	token = strtok( line, ":" );
	if ( strcmp( token, "Frame Time" ) != 0 )  goto bvh_error;
	token = strtok( NULL, separater );
	if ( token == NULL )  goto bvh_error;
	motionData.frame_time = atof( token );

	motionData.num_motion_channels = channels.size();
	motionData.data = new float[motionData.num_frames*motionData.num_motion_channels];

	for (uint i=0; i<motionData.num_frames; i++)
	{
		file.getline( line, BUFFER_LENGTH );
		token = strtok( line, separater );
		for ( uint j=0; j<motionData.num_motion_channels; j++ )
		{
			if (token == NULL)
				goto bvh_error;
			motionData.data[i*motionData.num_motion_channels+j] = atof(token);
			token = strtok( NULL, separater );
		}
	}

	file.close();
	load_success = true;
	return;

bvh_error:
	file.close();
}

JOINT* BVH::getJoint(std::string name)
{
	std::map<std::string, JOINT*>::const_iterator  i = nameToJoint.find(name);
	JOINT* j = (i!=nameToJoint.end()) ? (*i).second:NULL; 
	if(j==NULL){
		std::cout<<"JOINT <"<<name<<"> is not loaded!\n";
	}
	return j;
}

float* BVH::getMotionDataPtr(int frame_no)
{
	frame_no%=motionData.num_frames;
	return motionData.data+frame_no*motionData.num_motion_channels;
}

// Move joints using quarternion (PART 2B)
void BVH::moveJoint(JOINT* joint, float* mdata, float scale)
{
	// Rotation quaternion
	float rotationQuaternion[4];
	rotationQuaternion[0] = 1; // unit quaternion, so set one value to 1
	rotationQuaternion[1] = 0;
	rotationQuaternion[2] = 0;
	rotationQuaternion[3] = 0;

	// Compute initial translation component
	float translationQuaternion[4];
	translationQuaternion[0] = 0;
	translationQuaternion[1] = joint->offset.x * scale;
	translationQuaternion[2] = joint->offset.y * scale;
	translationQuaternion[3] = joint->offset.z * scale;

	float tempQuaternion[4];
	bool quaternionInit = false;
	for (uint i = 0; i<joint->channels.size(); i++)
	{
		// Extract value from motion data        
		CHANNEL *channel = joint->channels[i];
		float value = mdata[channel->index];
		switch (channel->type){
		case X_POSITION:
			translationQuaternion[1] += value * scale;
			break;
		case Y_POSITION:
			translationQuaternion[2] += value * scale;
			break;
		case Z_POSITION:
			translationQuaternion[3] += value * scale;
			break;
		case X_ROTATION:
			getQuarternion(value, 0, 0, tempQuaternion);
			if (!quaternionInit) {
				copyQuarternion(rotationQuaternion, tempQuaternion);
				quaternionInit = true;
			}
			else {
				multiplyQuarternion(rotationQuaternion, tempQuaternion);
			}
			break;
		case Y_ROTATION:
			getQuarternion(0, value, 0, tempQuaternion);
			if (!quaternionInit) {
				copyQuarternion(rotationQuaternion, tempQuaternion);
				quaternionInit = true;
			}
			else {
				multiplyQuarternion(rotationQuaternion, tempQuaternion);
			}
			break;
		case Z_ROTATION:
			getQuarternion(0, 0, value, tempQuaternion);
			if (!quaternionInit) {
				copyQuarternion(rotationQuaternion, tempQuaternion);
				quaternionInit = true;
			}
			else {
				multiplyQuarternion(rotationQuaternion, tempQuaternion);
			}
			break;
		}
	}

	// Update translation component
	translationQuaternion[1] *= 0.5;
	translationQuaternion[2] *= 0.5;
	translationQuaternion[3] *= 0.5;
	this->multiplyQuarternion(translationQuaternion, rotationQuaternion);

	// Combine with parent quaternion (if any)
	if (joint->parent == NULL) { // No parent, just copy over both
		this->copyQuarternion(joint->dualQuaternion[0], rotationQuaternion);
		this->copyQuarternion(joint->dualQuaternion[1], translationQuaternion);
	} else { // Multiply the dual quaternions between parent and local
		// Find the final rotation component
		float finalRotation[4];
		this->copyQuarternion(finalRotation, joint->parent->dualQuaternion[0]);
		this->multiplyQuarternion(finalRotation, rotationQuaternion);

		// Find the final translation component
		float term1[4], term2[4];
		this->copyQuarternion(term1, joint->parent->dualQuaternion[0]);
		this->copyQuarternion(term2, joint->parent->dualQuaternion[1]);
		this->multiplyQuarternion(term1, translationQuaternion);
		this->multiplyQuarternion(term2, rotationQuaternion);

		// Sum both translation terms
		term1[0] += term2[0];
		term1[1] += term2[1];
		term1[2] += term2[2];
		term1[3] += term2[3];

		// Assign the results
		this->copyQuarternion(joint->dualQuaternion[0], finalRotation);
		this->copyQuarternion(joint->dualQuaternion[1], term1);
	}

	// Get the rotation matrix
	glm::mat4 finalMatrix = this->getRotationMatrixFromQuarternion(joint->dualQuaternion[0]);
	
	// Get the translation vector
	float rotationConjugate[4];
	rotationConjugate[0] = joint->dualQuaternion[0][0];
	rotationConjugate[1] = -joint->dualQuaternion[0][1];
	rotationConjugate[2] = -joint->dualQuaternion[0][2];
	rotationConjugate[3] = -joint->dualQuaternion[0][3];
	float translationQuaternionResult[4];
	translationQuaternionResult[0] = 2 * joint->dualQuaternion[1][0];
	translationQuaternionResult[1] = 2 * joint->dualQuaternion[1][1];
	translationQuaternionResult[2] = 2 * joint->dualQuaternion[1][2];
	translationQuaternionResult[3] = 2 * joint->dualQuaternion[1][3];
	this->multiplyQuarternion(translationQuaternionResult, rotationConjugate);
	glm::vec3 translationVector;
	translationVector.x = translationQuaternionResult[1];
	translationVector.y = translationQuaternionResult[2];
	translationVector.z = translationQuaternionResult[3];

	// Get the translation matrix
	finalMatrix[3][0] = translationVector.x;
	finalMatrix[3][1] = translationVector.y;
	finalMatrix[3][2] = translationVector.z;

	// Set the joint matrix
	joint->matrix = finalMatrix;

	// do the same to all children
	for (std::vector<JOINT*>::iterator child = joint->children.begin(); child != joint->children.end(); ++child)
		moveJoint(*child, mdata, scale);
}

void BVH::moveTo(unsigned frame, float scale)
{
    // we calculate motion data's array start index for a frame
    unsigned start_index = frame * motionData.num_motion_channels;

    // recursively transform skeleton
    moveJoint(rootJoint, getMotionDataPtr(frame), scale);
}


// Get quarternion from the 3 euler angle representation
void BVH::getQuarternion(float adx, float ady, float adz, float q[]) {
	// Convert degrees to radians
	float ax = adx * M_PI / 180;
	float ay = ady * M_PI / 180;
	float az = adz * M_PI / 180;

	q[0] = cos(ax / 2) * cos(ay / 2) * cos(az / 2) + sin(ax / 2) * sin(ay / 2) * sin(az / 2);
	q[1] = sin(ax / 2) * cos(ay / 2) * cos(az / 2) - cos(ax / 2) * sin(ay / 2) * sin(az / 2);
	q[2] = cos(ax / 2) * sin(ay / 2) * cos(az / 2) + sin(ax / 2) * cos(ay / 2) * sin(az / 2);
	q[3] = cos(ax / 2) * cos(ay / 2) * sin(az / 2) - sin(ax / 2) * sin(ay / 2) * cos(az / 2);
}

// Find the rotation matrix from quarternion
glm::mat4 BVH::getRotationMatrixFromQuarternion(float q[]) {
	glm::mat4 m(1.0);
	float a = q[0], b = q[1], c = q[2], d = q[3];
	m[0][0] = a * a + b * b - c * c - d * d;
	m[1][0] = 2 * b * c - 2 * a * d;
	m[2][0] = 2 * b * d + 2 * a * c;
	m[0][1] = 2 * b * c + 2 * a * d;
	m[1][1] = a * a - b * b + c * c - d * d;
	m[2][1] = 2 * c * d - 2 * a * b;
	m[0][2] = 2 * b * d - 2 * a * c;
	m[1][2] = 2 * c * d + 2 * a * b;
	m[2][2] = a * a - b * b - c * c + d * d;
	return m;
}

// Multiplies q with r, altering replacing values of q with the result
void BVH::multiplyQuarternion(float q[], float r[]) {
	float result[4];
	result[0] = r[0] * q[0] - r[1] * q[1] - r[2] * q[2] - r[3] * q[3];
	result[1] = r[0] * q[1] + r[1] * q[0] - r[2] * q[3] + r[3] * q[2];
	result[2] = r[0] * q[2] + r[1] * q[3] + r[2] * q[0] - r[3] * q[1];
	result[3] = r[0] * q[3] - r[1] * q[2] + r[2] * q[1] + r[3] * q[0];

	q[0] = result[0];
	q[1] = result[1];
	q[2] = result[2];
	q[3] = result[3];
}

// Copy the values of r into q
void BVH::copyQuarternion(float q[], float r[]) {
	q[0] = r[0];
	q[1] = r[1];
	q[2] = r[2];
	q[3] = r[3];
}

void BVH::getEulerAnglesFromQuarternion(float q[], float v[]) {
	float w = q[0];
	float x = q[1];
	float y = q[2];
	float z = q[3];

	float sqw = w*w;
	float sqx = x*x;
	float sqy = y*y;
	float sqz = z*z;

	v[0] = (atan2f(2.0 * (y*z + x*w), (-sqx - sqy + sqz + sqw)) * (180.0f / M_PI));
	v[1] = (asinf(-2.0 * (x*z - y*w)) * (180.0f / M_PI));
	v[2] = (atan2f(2.0 * (x*y + z*w), (sqx - sqy - sqz + sqw)) * (180.0f / M_PI));
}

// Gets translation matrix for the joint (without parent transforms)
glm::mat4 BVH::getTranslationMatrix(JOINT* joint, int frame, float scale) {
	glm::mat4 ident(1.0f);

	ident = glm::translate(ident, glm::vec3(joint->offset.x *scale, 0, 0));
	ident = glm::translate(ident, glm::vec3(0, joint->offset.y * scale, 0));
	ident = glm::translate(ident, glm::vec3(0, 0, joint->offset.z * scale));

	return ident;
}

float BVH::toDegrees(float radians) {
	return radians / (2 * M_PI) * 360;
}