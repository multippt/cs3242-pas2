#ifndef _BVHANIMATOR_H
#define _BVHANIMATOR_H

#include "BVH.h"

class BVHAnimator
{
private:
	BVH* _bvh;
	//pointers used for rendering - 21 pointers
	JOINT *head,*neck,*chest,*spine,*hip;
	JOINT *lshldr,*larm,*lforearm,*lhand;
	JOINT *rshldr,*rarm,*rforearm,*rhand;
	JOINT *lupleg,*lleg,*lfoot,*ltoe;
	JOINT *rupleg,*rleg,*rfoot,*rtoe;
	std::vector<glm::vec4> vertices;
	std::vector<GLshort>   indices;
		
	// sets all pointers used for rendering
	void setpointers();
public:
	BVHAnimator(BVH* bvh);
    
	//set bvh
	void setBVH(BVH* bvh){_bvh = bvh;}
	
	/** draw the character **/
	//draw the character at frame_no
	void RenderFigure( int frame_no, float scale, int flag);	
	//render the character skeleton
	static void RenderSkeleton( const JOINT* root, const float* data, float scale = 1.0f );
	//render the character joints
	static void RenderJoints(const JOINT* root, const float* data, float scale = 1.0f);
	//render the character joints using LTM
	void RenderJointsLTM(int frame, float scale = 1.0f);
	//draw the mannequin
	void RenderMannequin(int frame_no, float scale = 1.0f);	

	void RenderMannequin(JOINT* joint, float* data, float scale);
	void RenderPart(float x0, float y0, float z0, float x1, float y1, float z1, std::string partStartName, std::string partEndName);

	/************************************************************************** 
	  Calculate and UPDATE JOINT's motion data by solving IK for left arm 
	  joints to position character's LeftHand to reach target (x,y,z)
	  Joints: LeftArm 3DOF, LeftForeArm 1DOF
	**************************************************************************/
	void solveLeftArm(int frame_no, float scale, float x, float y, float z);

	void solveAngles2D(float x, float y, float len1, float len2, float &theta1, float &theta2);
	void solveAngles1D(float x, float len1, float len2, float &theta1, float &theta2);

	glm::vec3 findProjectionOnPlane(glm::vec3 normal, glm::vec3 original, glm::vec3 point);
	float findRotationAngle(glm::mat4 transform, glm::vec4 endEffector, glm::vec4 target, int axis = 1);

	// Debug draw
	bool drawDebug;
	std::pair<glm::vec4, glm::vec4> debugLine;
	void drawDebugLine(glm::vec4 start, glm::vec4 end);
};

#endif