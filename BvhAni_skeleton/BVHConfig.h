#ifndef _BVHCONFIG_H_
#define _BVHCONFIG_H_

//the joint names
//as in nus mocap bvh

//precision used
#define _BVH_PRECISION_ 6

//spine
#define _BVH_ROOT_JOINT_ "Hips"
#define _BVH_SPINE_JOINT_ "Spine"
#define _BVH_CHEST_JOINT_ "Spine1"
#define _BVH_NECK_JOINT_ "Neck"
#define _BVH_HEAD_JOINT_ "Head"
//left arm
#define _BVH_L_SHOULDER_JOINT_ "LeftShoulder"
#define _BVH_L_ARM_JOINT_ "LeftArm"
#define _BVH_L_FOREARM_JOINT_ "LeftForeArm"
//left hand
#define _BVH_L_HAND_JOINT_ "LeftHand"
#define _BVH_L_HANDEND_JOINT_ "L_Wrist_End"
#define _BVH_L_HANDTHUMB_JOINT_ "LeftHandThumb"
//right arm
#define _BVH_R_SHOULDER_JOINT_ "RightShoulder"
#define _BVH_R_ARM_JOINT_ "RightArm"
#define _BVH_R_FOREARM_JOINT_ "RightForeArm"
//right hand
#define _BVH_R_HAND_JOINT_ "RightHand"
#define _BVH_R_HANDEND_JOINT_ "R_Wrist_End"
#define _BVH_R_HANDTHUMB_JOINT_ "RightHandThumb"
//left leg
#define _BVH_L_THIGH_JOINT_ "LeftUpLeg"
#define _BVH_L_SHIN_JOINT_ "LeftLeg"
#define _BVH_L_FOOT_JOINT_ "LeftFoot"
#define _BVH_L_TOE_JOINT_ "LeftToeBase"
//right leg
#define _BVH_R_THIGH_JOINT_ "RightUpLeg"
#define _BVH_R_SHIN_JOINT_ "RightLeg"
#define _BVH_R_FOOT_JOINT_ "RightFoot"
#define _BVH_R_TOE_JOINT_ "RightToeBase"

#endif