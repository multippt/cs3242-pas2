/** 
	Constructed based on a BVH player. - cuizh
**/

#ifndef  _BVH_H_
#define  _BVH_H_

#include <vector>
#include <string>
#include <map>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "GL/glut.h"
#include "BVHConfig.h"

enum  ChannelEnum
{
	X_ROTATION, Y_ROTATION, Z_ROTATION,
	X_POSITION, Y_POSITION, Z_POSITION
};

typedef unsigned int uint;

typedef struct{
	ChannelEnum type;
	uint index;
} CHANNEL;

typedef struct
{
	float x, y, z;
} OFFSET;

typedef struct JOINT JOINT;

struct JOINT
{
	std::string name;                     // joint name
	JOINT* parent;                        // joint parent	
	OFFSET offset;                        // offset data
	std::vector<CHANNEL*> channels;
	std::vector<JOINT*> children;         // joint's children
	glm::mat4 matrix;                     // local transofrmation matrix (premultiplied with parents')
	bool is_site;                         // if it is end site

	float dualQuaternion[2][4];			  // dual quaternion to represent translation and rotation
};

typedef struct
{
	unsigned int num_frames;              // number of frames
	unsigned int num_motion_channels;     // number of motion channels (of all joints per frame)
	float* data;                          // motion float data array
	float frame_time;                     // time per frame; FPS = 1/frame_time
} MOTION;

class BVH
{
private:
	JOINT* rootJoint;
	MOTION motionData;
	bool load_success;
	std::map<std::string,JOINT*> nameToJoint;
	std::vector<JOINT*> jointList;             //this list stores all pointers to joints
	std::vector<CHANNEL*> channels;

public:
	BVH();
	BVH(const char* filename);
	~BVH();

private:
	//write a joint info to bvh hierarchy part
	void writeJoint(JOINT* joint, std::ostream& stream,int level);			
	// clean up stuff, used in destructor
	void clear();

public:
	/************************************************************************** 
	  functions for basic I/O - only loading is needed
	**************************************************************************/
	// load a bvh file
	void load(const std::string& filename);  	
	// is the bvh file successfully loaded? 
	bool IsLoadSuccess() const {return load_success;}

	/************************************************************************** 
	  functions to retrieve bvh data 
	**************************************************************************/
	// get root joint
	JOINT* getRootJoint(){ return rootJoint;}
	// get the JOINT pointer from joint name 
	JOINT* getJoint(std::string name);	
	// get the pointer to mation data at frame_no
	//       note: frame_no is treated as frame_no%total_frames
	float* getMotionDataPtr(int frame_no);
	// get a pointer to the root joint 
	const JOINT* getRootJoint() const { return rootJoint; }
	// get the list of the joint
	const std::vector<JOINT*> getJointList() const {return jointList; }
	// get the number of frames 
	unsigned getNumFrames() const { return motionData.num_frames; }
	// get time per frame
	float getFrameTime() const { return motionData.frame_time; }	

	/************************************************************************** 
	  Calculate JOINT's transformation matrix for specified frame index 
	**************************************************************************/
	// calculate the local transformation matrix for the joint based on motion data
	void moveJoint(JOINT* joint, float* mdata, float scale);

	// loads motion data from a frame into local matrices
	void moveTo(unsigned frame, float scale);

	/** Quarternion functions **/
	void getQuarternion(float adx, float ady, float adz, float q[]);
	glm::mat4 getRotationMatrixFromQuarternion(float q[]);
	void multiplyQuarternion(float q[], float r[]);
	void copyQuarternion(float q[], float r[]);
	glm::mat4 getTranslationMatrix(JOINT* joint, int frame, float scale);
	void getEulerAnglesFromQuarternion(float q[], float v[]);
	float toDegrees(float radians);
};
#endif