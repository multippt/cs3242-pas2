#define _USE_MATH_DEFINES

#include "BVHAnimator.h"
#include <iostream> // for debugging - cuizh

//color used for rendering, in RGB
float color[3];

//function to convert a glm::mat4 matrix to GLdouble[16]
void mat4toGLdouble16(GLdouble* m, glm::mat4 mat){
	//be careful about the order
	for(uint i=0;i<4;i++)
		for(uint j=0;j<4;j++)
			m[i*4+j] = mat[i][j];
}

//draw a sphere located at (x,y,z) of radius (size)
void RenderSphere( float x, float y, float z, float size){		
	float radius = size;
	int numSlices = 32;
	int numStacks = 8; 
	static GLUquadricObj *quad_obj = gluNewQuadric();
	gluQuadricDrawStyle( quad_obj, GLU_FILL );
	gluQuadricNormals( quad_obj, GLU_SMOOTH );

	glPushMatrix();	
	glTranslated( x, y, z );

	glColor3f(color[0],color[1],color[2]);

	gluSphere(quad_obj,radius,numSlices,numStacks);	

	glPopMatrix();
}

//draw a bone from (x0,y0,z0) to (x1,y1,z1) as a cylinder of radius (boneSize)
void RenderBone( float x0, float y0, float z0, float x1, float y1, float z1, float boneSize ){
	GLdouble  dir_x = x1 - x0;
	GLdouble  dir_y = y1 - y0;
	GLdouble  dir_z = z1 - z0;
	GLdouble  bone_length = sqrt( dir_x*dir_x + dir_y*dir_y + dir_z*dir_z );

	static GLUquadricObj *  quad_obj = NULL;
	if ( quad_obj == NULL )
		quad_obj = gluNewQuadric();	
	gluQuadricDrawStyle( quad_obj, GLU_FILL );
	gluQuadricNormals( quad_obj, GLU_SMOOTH );

	glPushMatrix();

	glTranslated( x0, y0, z0 );

	double  length;
	length = sqrt( dir_x*dir_x + dir_y*dir_y + dir_z*dir_z );
	if ( length < 0.0001 ) { 
		dir_x = 0.0; dir_y = 0.0; dir_z = 1.0;  length = 1.0;
	}
	dir_x /= length;  dir_y /= length;  dir_z /= length;

	GLdouble  up_x, up_y, up_z;
	up_x = 0.0;
	up_y = 1.0;
	up_z = 0.0;

	double  side_x, side_y, side_z;
	side_x = up_y * dir_z - up_z * dir_y;
	side_y = up_z * dir_x - up_x * dir_z;
	side_z = up_x * dir_y - up_y * dir_x;

	length = sqrt( side_x*side_x + side_y*side_y + side_z*side_z );
	if ( length < 0.0001 ) {
		side_x = 1.0; side_y = 0.0; side_z = 0.0;  length = 1.0;
	}
	side_x /= length;  side_y /= length;  side_z /= length;

	up_x = dir_y * side_z - dir_z * side_y;
	up_y = dir_z * side_x - dir_x * side_z;
	up_z = dir_x * side_y - dir_y * side_x;

	GLdouble  m[16] = { side_x, side_y, side_z, 0.0,
	                    up_x,   up_y,   up_z,   0.0,
	                    dir_x,  dir_y,  dir_z,  0.0,
	                    0.0,    0.0,    0.0,    1.0 };
	glMultMatrixd( m );

	GLdouble radius= boneSize; 
	GLdouble slices = 8.0; 
	GLdouble stack = 3.0;  
	glColor3f(color[0],color[1],color[2]);
	gluCylinder( quad_obj, radius, radius, bone_length, slices, stack ); 

	glPopMatrix();
}

BVHAnimator::BVHAnimator(BVH* bvh)
{
	_bvh = bvh;
	setpointers();
}

void  BVHAnimator::RenderFigure( int frame_no, float scale, int flag )
{	
	switch(flag){
	case 0:
		//RenderJoints( _bvh->getRootJoint(), _bvh->getMotionDataPtr(frame_no), scale);
		// via LTM [PART 2B-1]
		RenderJointsLTM(frame_no,scale);
		break;
	case 1:
		RenderSkeleton( _bvh->getRootJoint(), _bvh->getMotionDataPtr(frame_no), scale );	
		break;
	case 2:	
		RenderMannequin(frame_no,scale);
		break;
	default:
		break;
	}
}

void  BVHAnimator::RenderSkeleton( const JOINT* joint, const float*data, float scale )
{	
	color[0]=1.;color[1]=0.;color[2]=0.;
	float bonesize = 0.03;
	glPushMatrix();
	//translate
	if ( joint->parent == NULL )//root
	{
		glTranslatef( data[0] * scale, data[1] * scale, data[2] * scale );
	}
	else
	{
		glTranslatef( joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale );
	}
	//rotate
	for ( uint i=0; i<joint->channels.size(); i++ )
	{
		CHANNEL *channel = joint->channels[i];
		if ( channel->type == X_ROTATION )
			glRotatef( data[channel->index], 1.0f, 0.0f, 0.0f );
		else if ( channel->type == Y_ROTATION )
			glRotatef( data[channel->index], 0.0f, 1.0f, 0.0f );
		else if ( channel->type == Z_ROTATION )
			glRotatef( data[channel->index], 0.0f, 0.0f, 1.0f );
	}
	//end site? skip!
	if ( joint->children.size() == 0 && !joint->is_site)
	{
		RenderBone(0.0f, 0.0f, 0.0f, joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale,bonesize);
	}
	if ( joint->children.size() == 1 )
	{
		JOINT *  child = joint->children[ 0 ];
		RenderBone(0.0f, 0.0f, 0.0f, child->offset.x*scale, child->offset.y*scale, child->offset.z*scale,bonesize);
	}
	if ( joint->children.size() > 1 )
	{
		float  center[ 3 ] = { 0.0f, 0.0f, 0.0f };
		for ( uint i=0; i<joint->children.size(); i++ )
		{
			JOINT *  child = joint->children[i];
			center[0] += child->offset.x;
			center[1] += child->offset.y;
			center[2] += child->offset.z;
		}
		center[0] /= joint->children.size() + 1;
		center[1] /= joint->children.size() + 1;
		center[2] /= joint->children.size() + 1;

		RenderBone(	0.0f, 0.0f, 0.0f, center[0]*scale, center[1]*scale, center[2]*scale,bonesize);

		for ( uint i=0; i<joint->children.size(); i++ )
		{
			JOINT *  child = joint->children[i];
			RenderBone(	center[0]*scale, center[1]*scale, center[2]*scale,
				child->offset.x*scale, child->offset.y*scale, child->offset.z*scale,bonesize);
		}
	}
	//recursively render all bones
	for ( uint i=0; i<joint->children.size(); i++ )
	{
		RenderSkeleton( joint->children[ i ], data, scale );
	}
	glPopMatrix();
}

void  BVHAnimator::RenderJoints( const JOINT* joint, const float*data, float scale )
{	
	color[0] = 1.;color[1] = 0.;color[2] = 0.;
	glPushMatrix();
	//translate
	if ( joint->parent == NULL )//root
	{
		glTranslatef( data[0] * scale, data[1] * scale, data[2] * scale );
	}
	else
	{
		glTranslatef( joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale );
	}

	// Rotate the joints [PART 2A]
	for (uint i = 0; i<joint->channels.size(); i++)
	{
		CHANNEL *channel = joint->channels[i];
		if (channel->type == X_ROTATION)
			glRotatef(data[channel->index], 1.0f, 0.0f, 0.0f);
		else if (channel->type == Y_ROTATION)
			glRotatef(data[channel->index], 0.0f, 1.0f, 0.0f);
		else if (channel->type == Z_ROTATION)
			glRotatef(data[channel->index], 0.0f, 0.0f, 1.0f);
	}

	//end site
	if ( joint->children.size() == 0 )
	{
		RenderSphere(joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale,0.07);
	}
	if ( joint->children.size() == 1 )
	{
		JOINT *  child = joint->children[ 0 ];
		RenderSphere(child->offset.x*scale, child->offset.y*scale, child->offset.z*scale,0.07);
	}
	if ( joint->children.size() > 1 )
	{
		float  center[ 3 ] = { 0.0f, 0.0f, 0.0f };
		for ( uint i=0; i<joint->children.size(); i++ )
		{
			JOINT *  child = joint->children[i];
			center[0] += child->offset.x;
			center[1] += child->offset.y;
			center[2] += child->offset.z;
		}
		center[0] /= joint->children.size() + 1;
		center[1] /= joint->children.size() + 1;
		center[2] /= joint->children.size() + 1;

		RenderSphere(center[0]*scale, center[1]*scale, center[2]*scale,0.07);

		for ( uint i=0; i<joint->children.size(); i++ )
		{
			JOINT *  child = joint->children[i];
			RenderSphere(child->offset.x*scale, child->offset.y*scale, child->offset.z*scale,0.07);
		}
	}
	//recursively render all joints
	for ( uint i=0; i<joint->children.size(); i++ )
	{
		RenderJoints( joint->children[i], data, scale );
	}
	glPopMatrix();
}

void  BVHAnimator::RenderJointsLTM( int frame, float scale )
{
	_bvh->moveTo(frame,scale);
	std::vector<JOINT*> jointList = _bvh->getJointList();	
	for(std::vector<JOINT*>::iterator it=jointList.begin();it!=jointList.end();it++)
	{
		glPushMatrix();	
		glm::mat4 mat = (*it)->matrix;
		GLdouble  m[16];
		mat4toGLdouble16(m,mat);
		glMultMatrixd(m);
		if((*it)->is_site){
			color[0]=0.;color[1]=0.;color[2]=1.;
			RenderSphere(0,0,0,0.04);
		}
		else{
			color[0]=0.;color[1]=1.;color[2]=0.;
			RenderSphere(0,0,0,0.07);
		}
		glPopMatrix();
	}	
}

void BVHAnimator::setpointers(){
	head = _bvh->getJoint(std::string(_BVH_HEAD_JOINT_));
	neck = _bvh->getJoint(std::string(_BVH_NECK_JOINT_));
	chest = _bvh->getJoint(std::string(_BVH_CHEST_JOINT_));
	spine = _bvh->getJoint(std::string(_BVH_SPINE_JOINT_));
	hip = _bvh->getJoint(std::string(_BVH_ROOT_JOINT_)); //root joint

	lshldr = _bvh->getJoint(std::string(_BVH_L_SHOULDER_JOINT_));
	larm = _bvh->getJoint(std::string(_BVH_L_ARM_JOINT_));
	lforearm = _bvh->getJoint(std::string(_BVH_L_FOREARM_JOINT_));
	lhand = _bvh->getJoint(std::string(_BVH_L_HAND_JOINT_));

	rshldr = _bvh->getJoint(std::string(_BVH_R_SHOULDER_JOINT_));
	rarm = _bvh->getJoint(std::string( _BVH_R_ARM_JOINT_));
	rforearm = _bvh->getJoint(std::string(_BVH_R_FOREARM_JOINT_));
	rhand = _bvh->getJoint(std::string(_BVH_R_HAND_JOINT_));

	lupleg = _bvh->getJoint(std::string(_BVH_L_THIGH_JOINT_));
	lleg = _bvh->getJoint(std::string(_BVH_L_SHIN_JOINT_));
	lfoot = _bvh->getJoint(std::string(_BVH_L_FOOT_JOINT_));
	ltoe = _bvh->getJoint(std::string(_BVH_L_TOE_JOINT_));

	rupleg = _bvh->getJoint(std::string(_BVH_R_THIGH_JOINT_));
	rleg = _bvh->getJoint(std::string(_BVH_R_SHIN_JOINT_));
	rfoot = _bvh->getJoint(std::string(_BVH_R_FOOT_JOINT_));
	rtoe = _bvh->getJoint(std::string(_BVH_R_TOE_JOINT_));
}

void getScaledOffset(OFFSET& c, OFFSET a, OFFSET b, float scale)
{
	c.x = (a.x-b.x)*scale;
	c.y = (a.y-b.y)*scale;
	c.z = (a.z-b.z)*scale;
}

// Render mannequin (PART 2C)
void BVHAnimator::RenderMannequin(int frame, float scale){
	//You may simply draw a couple of basic geometries to build the mannequin using the RenderSphere(��) and RenderBone(��) provided in BVHAnimator.cpp or GL functions like glutSolidCube(��), etc.

	JOINT* joint = _bvh->getRootJoint();
	float* data = _bvh->getMotionDataPtr(frame);
	RenderMannequin(joint, data, scale);
}

void BVHAnimator::RenderMannequin(JOINT* joint, float* data, float scale) {
	color[0] = 1.; color[1] = 0.; color[2] = 0.;
	float bonesize = 0.03;
	glPushMatrix();
	//translate
	if (joint->parent == NULL)//root
	{
		glTranslatef(data[0] * scale, data[1] * scale, data[2] * scale);
	}
	else
	{
		glTranslatef(joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale);
	}
	//rotate
	for (uint i = 0; i<joint->channels.size(); i++)
	{
		CHANNEL *channel = joint->channels[i];
		if (channel->type == X_ROTATION)
			glRotatef(data[channel->index], 1.0f, 0.0f, 0.0f);
		else if (channel->type == Y_ROTATION)
			glRotatef(data[channel->index], 0.0f, 1.0f, 0.0f);
		else if (channel->type == Z_ROTATION)
			glRotatef(data[channel->index], 0.0f, 0.0f, 1.0f);
	}
	//end site? skip!
	if (joint->children.size() == 0 && !joint->is_site)
	{
		RenderPart(0.0f, 0.0f, 0.0f, joint->offset.x*scale, joint->offset.y*scale, joint->offset.z*scale, joint->name, joint->name);
	}
	if (joint->children.size() == 1)
	{
		JOINT *  child = joint->children[0];
		RenderPart(0.0f, 0.0f, 0.0f, child->offset.x*scale, child->offset.y*scale, child->offset.z*scale, joint->name, child->name);
	}
	if (joint->children.size() > 1)
	{
		float  center[3] = { 0.0f, 0.0f, 0.0f };
		for (uint i = 0; i<joint->children.size(); i++)
		{
			JOINT *  child = joint->children[i];
			center[0] += child->offset.x;
			center[1] += child->offset.y;
			center[2] += child->offset.z;
		}
		center[0] /= joint->children.size() + 1;
		center[1] /= joint->children.size() + 1;
		center[2] /= joint->children.size() + 1;

		RenderPart(0.0f, 0.0f, 0.0f, center[0] * scale, center[1] * scale, center[2] * scale, joint->name, joint->name);

		for (uint i = 0; i<joint->children.size(); i++)
		{
			JOINT *  child = joint->children[i];
			RenderPart(center[0] * scale, center[1] * scale, center[2] * scale,
				child->offset.x*scale, child->offset.y*scale, child->offset.z*scale, joint->name, child->name);
		}
	}
	//recursively render all bones
	for (uint i = 0; i<joint->children.size(); i++)
	{
		RenderMannequin(joint->children[i], data, scale);
	}
	glPopMatrix();
}

// Draw a mannequin part
void BVHAnimator::RenderPart(float x0, float y0, float z0, float x1, float y1, float z1, std::string partStartName, std::string partEndName){
	// Defaults
	float partSize = 0.03f;
	float partColor[3];
	partColor[0] = color[0];
	partColor[1] = color[1];
	partColor[2] = color[2];
	float partScaleX = 1.2f;
	float partScaleY = 1.2f;
	float partScaleZ = 1.2f;
	bool drawJoint = false;

	GLdouble  dir_x = x1 - x0;
	GLdouble  dir_y = y1 - y0;
	GLdouble  dir_z = z1 - z0;
	GLdouble  bone_length = sqrt(dir_x*dir_x + dir_y*dir_y + dir_z*dir_z);

	// Customize the parts based on name
	if (partStartName == "Hips" && (partEndName == "LeftUpLeg" || partEndName == "RightUpLeg")) { // *Root bone, not visible
		// Hidden
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.06f;
		return;
	} else if ((partStartName == "LeftUpLeg" || partStartName == "RightUpLeg") && (partEndName == "LeftLeg" || partEndName == "RightLeg")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.06f;
		drawJoint = true;
	} else if ((partStartName == "LeftLeg" || partStartName == "RightLeg") && (partEndName == "LeftFoot" || partEndName == "RightFoot")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.06f;
		drawJoint = true;
	} else if ((partStartName == "LeftFoot" || partStartName == "RightFoot") && (partEndName == "LeftToeBase" || partEndName == "RightToeBase")) {
		partColor[0] = 0.75f;
		partColor[1] = 0.4f;
		partColor[2] = 0.1f;
		partSize = 0.05f;
	} else if ((partStartName == "LeftToeBase" || partStartName == "RightToeBase") && (partEndName == "EndSite")) {
		partColor[0] = 0.75f;
		partColor[1] = 0.4f;
		partColor[2] = 0.1f;
		partSize = 0.04f;
	} else if (partStartName == "Hips" && (partEndName == "Spine")) {
		return; // Not visible
	} else if (partStartName == "Hips" && (partEndName == "Hips")) {
		return; // Not visible
	} else if (partStartName == "Spine" && (partEndName == "Spine1")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.1f;
	} else if (partStartName == "Spine1" && (partEndName == "Spine1")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.1f;
		partScaleX = 1.5f;
		partScaleY = 1.5f;
		partScaleZ = 1.5f;
	} else if (partStartName == "Spine1" && (partEndName == "Neck")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.06f;
		partScaleX = 4.0f;
		partScaleY = 2.0f;
		partScaleZ = 4.0f;
	} else if (partStartName == "Neck" && (partEndName == "Head")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.05f;
	} else if (partStartName == "Head" && (partEndName == "EndSite")) {
		partColor[0] = 1.0f;
		partColor[1] = 0.5f;
		partColor[2] = 0.2f;
		partSize = 0.5 * bone_length;
		partScaleX = 2.0f;
		partScaleY = 2.0f;
		partScaleZ = 2.0f;
	} else if (partStartName == "Spine1" && (partEndName == "LeftShoulder" || partEndName == "RightShoulder")) {
		return; // Hidden
	} else  if ((partStartName == "LeftShoulder" || partStartName == "RightShoulder") && (partEndName == "LeftArm" || partEndName == "RightArm")) {
		return; // Hidden
	} else if ((partStartName == "LeftArm" || partStartName == "RightArm") && (partEndName == "LeftForeArm" || partEndName == "RightForeArm")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.07f;
		drawJoint = true;
	} else if ((partStartName == "LeftForeArm" || partStartName == "RightForeArm") && (partEndName == "LeftHand" || partEndName == "RightHand")) {
		partColor[0] = 0.5f;
		partColor[1] = 0.75f;
		partColor[2] = 1.0f;
		partSize = 0.06f;
		drawJoint = true;
	} else if ((partStartName == "LeftHand" || partStartName == "RightHand") && (partEndName == "LeftHandThumb" || partEndName == "RightHandThumb")) {
		return; // Hidden
	} else if ((partStartName == "LeftHand" || partStartName == "RightHand") && (partEndName == "LeftHand" || partEndName == "RightHand")) {
		return; // Hidden
	} else if ((partStartName == "LeftHand" || partStartName == "RightHand") && (partEndName == "L_Wrist_End" || partEndName == "R_Wrist_End")) {
		partColor[0] = 0.75f;
		partColor[1] = 0.4f;
		partColor[2] = 0.1f;
		partSize = 0.04f;
		partScaleX = 1.5f;
		partScaleY = 0.8f;
		partScaleZ = 2.0f;
	} else if ((partStartName == "LeftHandThumb" || partStartName == "RightHandThumb") && (partEndName == "EndSite")) {
		return; // Hidden
	} else if ((partStartName == "L_Wrist_End" || partStartName == "R_Wrist_End") && (partEndName == "L_Wrist_End" || partEndName == "R_Wrist_End")) {
		return; // Hidden
	} else {
		printf("Uncolored part: %s, %s\n", partStartName.c_str(), partEndName.c_str());
	}

	static GLUquadricObj *  quad_obj = NULL;
	if (quad_obj == NULL)
		quad_obj = gluNewQuadric();
	gluQuadricDrawStyle(quad_obj, GLU_FILL);
	gluQuadricNormals(quad_obj, GLU_SMOOTH);

	glPushMatrix();

	glTranslated(x0, y0, z0);

	double  length;
	length = sqrt(dir_x*dir_x + dir_y*dir_y + dir_z*dir_z);
	if (length < 0.0001) {
		dir_x = 0.0; dir_y = 0.0; dir_z = 1.0;  length = 1.0;
	}
	dir_x /= length;  dir_y /= length;  dir_z /= length;

	GLdouble  up_x, up_y, up_z;
	up_x = 0.0;
	up_y = 1.0;
	up_z = 0.0;

	double  side_x, side_y, side_z;
	side_x = up_y * dir_z - up_z * dir_y;
	side_y = up_z * dir_x - up_x * dir_z;
	side_z = up_x * dir_y - up_y * dir_x;

	length = sqrt(side_x*side_x + side_y*side_y + side_z*side_z);
	if (length < 0.0001) {
		side_x = 1.0; side_y = 0.0; side_z = 0.0;  length = 1.0;
	}
	side_x /= length;  side_y /= length;  side_z /= length;

	up_x = dir_y * side_z - dir_z * side_y;
	up_y = dir_z * side_x - dir_x * side_z;
	up_z = dir_x * side_y - dir_y * side_x;

	GLdouble  m[16] = { side_x, side_y, side_z, 0.0,
		up_x, up_y, up_z, 0.0,
		dir_x, dir_y, dir_z, 0.0,
		0.0, 0.0, 0.0, 1.0 };
	glMultMatrixd(m);

	

	GLdouble radius = partSize;
	GLdouble slices = 8.0;
	GLdouble stack = 3.0;
	glColor3f(partColor[0], partColor[1], partColor[2]);
	//glColor3f(color[0], color[1], color[2]);
	//gluCylinder(quad_obj, radius, radius, bone_length, slices, stack);
	

	stack = 8.0;
	glPushMatrix();

	// Draw joint
	if (drawJoint) {
		gluSphere(quad_obj, radius * 1.2, slices, stack);
	}

	glTranslatef(0, 0, bone_length / 2);
	//glScalef(radius, radius, bone_length / 2);

	glScalef(1, 1, bone_length / (2 * radius));
	glScalef(partScaleX, partScaleY, partScaleZ); // scale a bit more to overlap
	gluSphere(quad_obj, radius, slices, stack);
	glPopMatrix();

	glPopMatrix();
}

void BVHAnimator::solveLeftArm(int frame_no, float scale, float x, float y, float z)
{
	_bvh->moveTo(frame_no,scale);

	float *LArx, *LAry, *LArz, *LFAry;
	
	float *mdata = _bvh->getMotionDataPtr(0);
	//3 channels - Xrotation, Yrotation, Zrotation
    //extract value address from motion data        
    CHANNEL *channel = larm->channels[0]; //load left shoulder channel data 
	LArx = &mdata[channel->index];
	channel = larm->channels[1];
	LAry = &mdata[channel->index];
	channel = larm->channels[2];
	LArz = &mdata[channel->index];

	channel = lforearm->channels[1]; //load left elbow channel data
	LFAry = &mdata[channel->index];
	
	// [Assignment 2] - Implement an IK solver below

	// Calculate the matrix for larm, without its rotation matrix applied 
	// (because rotation matrix would be altered during computation)
	glm::mat4 larmMat = larm->parent->matrix * _bvh->getTranslationMatrix(larm, frame_no, scale);

	JOINT* lhand = lforearm->children.at(0); // Actual length of forearm is in here
	JOINT* lwrist = lhand->children.at(1); // Consider wrist

	glm::vec4 target(x, y, z, 1);

	// Compute translation and rotation matrices
	glm::mat4 larmTranslate = _bvh->getTranslationMatrix(lforearm, frame_no, scale);
	glm::mat4 lfarmTranslate = _bvh->getTranslationMatrix(lhand, frame_no, scale);
	glm::mat4 larmRotate(1.0f);
	glm::mat4 lfarmRotate(1.0f);

	// Account for wrist (ball to intersect middle of palm)
	glm::mat4 lwristTranslate(1.0f);
	lwristTranslate = glm::translate(lwristTranslate, glm::vec3(lwrist->offset.x *scale / 2, 0, 0));
	lfarmTranslate = lfarmTranslate * lwristTranslate;

	// Initialize to rest angles
	*LArx = 0;
	*LAry = 0;
	*LArz = 0;
	*LFAry = 0;

	// Quarternions
	float rotationQuaternionLArm[4];
	float rotationQuaternionLFArm[4];

	// Temporary quarternions
	float larmquaternionX[4];
	float larmquaternionY[4];
	float larmquaternionZ[4];
	
	glm::vec4 endPoint;
	glm::mat4 transform;

	/* // From observations, better results are obtained if initialized from rest pose (all angles zero)
	// Calculate rotational information from last pose
	// For lower forearm
	_bvh->getQuarternion(0, *LFAry, 0, rotationQuaternionLFArm);
	lfarmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLFArm);

	// For lower arm
	_bvh->getQuarternion(0, 0, 0, rotationQuaternionLArm);
	_bvh->getQuarternion(*LArx, 0, 0, larmquaternionX);
	_bvh->getQuarternion(0, *LAry, 0, larmquaternionY);
	_bvh->getQuarternion(0, 0, *LArz, larmquaternionZ);
	_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionX);
	_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionY);
	_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionZ);
	larmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLArm);*/

	// Record the deltas
	float deltaLFAry = 0;
	float deltaLArx = 0;
	float deltaLAry = 0;
	float deltaLArz = 0;

	// Solve using Cyclic Coordinate Descent (CCD)
	int iterations = 10; // Ten is roughly sufficient to give a good approximation (anything more probably means target is out of reach)
	for (int i = 0; i < iterations; i++) {
		/* Rotate about y-axis for elbow */
		endPoint = glm::vec4(0, 0, 0, 1);
		endPoint = larmMat * larmRotate * larmTranslate * lfarmRotate * lfarmTranslate * endPoint; // Find end effector
		transform = larmMat * larmRotate * larmTranslate * lfarmRotate;

		deltaLFAry = this->findRotationAngle(transform, endPoint, target);
		*LFAry += deltaLFAry;
		
		// Update rotation matrix
		_bvh->getQuarternion(0, *LFAry, 0, rotationQuaternionLFArm);
		lfarmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLFArm);


		/* Rotate about y-axis for arm */
		endPoint = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		endPoint = larmMat * larmRotate * larmTranslate * lfarmRotate * lfarmTranslate * endPoint; // Find end effector
		transform = larmMat * larmRotate;

		deltaLAry = this->findRotationAngle(transform, endPoint, target);
		*LAry += deltaLAry;
		
		// Update rotation matrix
		_bvh->getQuarternion(0, 0, 0, rotationQuaternionLArm);
		_bvh->getQuarternion(*LArx, 0, 0, larmquaternionX);
		_bvh->getQuarternion(0, *LAry, 0, larmquaternionY);
		_bvh->getQuarternion(0, 0, *LArz, larmquaternionZ);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionX);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionY);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionZ);
		larmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLArm);

		/* Rotate about x-axis for arm*/
		endPoint = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		endPoint = larmMat * larmRotate * larmTranslate * lfarmRotate * lfarmTranslate * endPoint; // Find end effector
		transform = larmMat * larmRotate;

		deltaLArx = this->findRotationAngle(transform, endPoint, target, 0);
		*LArx += deltaLArx;

		// Update rotation matrix
		_bvh->getQuarternion(0, 0, 0, rotationQuaternionLArm);
		_bvh->getQuarternion(*LArx, 0, 0, larmquaternionX);
		_bvh->getQuarternion(0, *LAry, 0, larmquaternionY);
		_bvh->getQuarternion(0, 0, *LArz, larmquaternionZ);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionX);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionY);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionZ);
		larmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLArm);


		/* Rotate about z-axis for arm*/
		endPoint = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		endPoint = larmMat * larmRotate * larmTranslate * lfarmRotate * lfarmTranslate * endPoint; // Find end effector
		transform = larmMat * larmRotate;

		deltaLArz = this->findRotationAngle(transform, endPoint, target, 2);
		*LArz += deltaLArz;

		// Update rotation matrix
		_bvh->getQuarternion(0, 0, 0, rotationQuaternionLArm);
		_bvh->getQuarternion(*LArx, 0, 0, larmquaternionX);
		_bvh->getQuarternion(0, *LAry, 0, larmquaternionY);
		_bvh->getQuarternion(0, 0, *LArz, larmquaternionZ);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionX);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionY);
		_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionZ);
		larmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLArm);

		// All deltas zero, so can end early since it won't change further
		float delta = deltaLFAry*deltaLFAry + deltaLArx*deltaLArx + deltaLAry*deltaLAry + deltaLArz*deltaLArz;
		if (delta > -0.0001 && delta < 0.0001) {
			endPoint = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			endPoint = larmMat * larmRotate * larmTranslate * lfarmRotate * lfarmTranslate * endPoint;
			float distance = glm::distance(endPoint, target); // Distance between end effector and target
			if (distance > 0.0001) { 
				// This occurs if local minima met on all joints and end effector didn't reach target
				deltaLAry = 45.0f; // Nudge one joint (Larm y-axis) to break out of minima

				// Update rotation matrix
				*LAry += deltaLAry;
				_bvh->getQuarternion(0, 0, 0, rotationQuaternionLArm);
				_bvh->getQuarternion(*LArx, 0, 0, larmquaternionX);
				_bvh->getQuarternion(0, *LAry, 0, larmquaternionY);
				_bvh->getQuarternion(0, 0, *LArz, larmquaternionZ);
				_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionX);
				_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionY);
				_bvh->multiplyQuarternion(rotationQuaternionLArm, larmquaternionZ);
				larmRotate = _bvh->getRotationMatrixFromQuarternion(rotationQuaternionLArm);
			} else {
				break; // IK solution found
			}
		}
	}
}

// Analytic method for solving
void BVHAnimator::solveAngles2D(float x, float y, float len1, float len2, float &theta1, float &theta2) {
	float xe = x;
	float ye = y;
	theta1 = 0.0f;
	theta2 = 0.0f;

	//float thetar = atan2f(-z, x);
	float thetar = atan2f(-ye, xe);

	// Using new ref
	theta2 = 2 * atanf(sqrt(((len1 + len2) * (len1 + len2) - (xe * xe + ye * ye)) / ((xe * xe + ye * ye) - (len1 - len2) * (len1 - len2))));
	theta2 = -theta2; // theta2 has two solutions, pick the negation
	float phi = atan2f(len2 * sin(theta2), len1 + len2 * cos(theta2));
	theta1 = thetar - phi;
}

void BVHAnimator::solveAngles1D(float x, float len1, float len2, float &theta1, float &theta2) {
	float theta = acos((len1*len1 + len2*len2 - x*x) / (2 * len1 * len2));
	float pretheta1 = len1 * sin(theta) / x;
	//float pretheta2 = len2 * sin(theta) / x;
	theta1 = pretheta1;
	theta2 = 0;
	theta2 = -(M_PI - theta);
}

glm::vec3 BVHAnimator::findProjectionOnPlane(glm::vec3 normal, glm::vec3 original, glm::vec3 point) {
	glm::vec3 result = point - glm::dot(point - original, normal) * normal;
	return result;
}

void BVHAnimator::drawDebugLine(glm::vec4 start, glm::vec4 end) {
	this->drawDebug = true;
	this->debugLine.first = start;
	this->debugLine.second = end;
}

// Finds the angle between endEffector and target (in degrees), projected onto selected plane
// transform: The transformation matrix to reach the joint (including rotation)
float BVHAnimator::findRotationAngle(glm::mat4 transform, glm::vec4 endEffector, glm::vec4 target, int axis) {
	glm::vec4 intPoint(0, 0, 0, 1);
	intPoint = transform * intPoint; // Joint coordinate

	// Get axis vectors
	glm::vec4 axisx(1, 0, 0, 1);
	glm::vec4 axisy(0, 1, 0, 1);
	glm::vec4 axisz(0, 0, 1, 1);
	axisx = transform * axisx - intPoint;
	axisy = transform * axisy - intPoint;
	axisz = transform * axisz - intPoint;

	glm::vec3 axisx3(axisx.x, axisx.y, axisx.z);
	glm::vec3 axisy3(axisy.x, axisy.y, axisy.z);
	glm::vec3 axisz3(axisz.x, axisz.y, axisz.z);
	axisx3 = axisx3 / glm::length(axisx3);
	axisy3 = axisy3 / glm::length(axisy3);
	axisz3 = axisz3 / glm::length(axisz3);

	// Project onto x-z plane
	glm::vec3 intPoint3a(intPoint.x, intPoint.y, intPoint.z);
	glm::vec3 target3a(target.x, target.y, target.z);
	glm::vec3 endPoint3a(endEffector.x, endEffector.y, endEffector.z);

	glm::vec3 chosenAxisX, chosenAxisY;
	switch (axis) { // Pick axis based on rotation axis
	case 0: // Rotate about x-axis
		chosenAxisX = -axisy3;
		chosenAxisY = axisz3;
		break;
	case 1: // Rotate about y-axis
		chosenAxisX = axisx3;
		chosenAxisY = axisz3;
		break;
	case 2: // Rotate about z-axis
		chosenAxisX = axisx3;
		chosenAxisY = -axisy3;
		break;
	}

	// Project along interested axis
	target3a = glm::vec3(glm::dot(target3a, chosenAxisX), glm::dot(target3a, chosenAxisY), 0);
	intPoint3a = glm::vec3(glm::dot(intPoint3a, chosenAxisX), glm::dot(intPoint3a, chosenAxisY), 0);
	endPoint3a = glm::vec3(glm::dot(endPoint3a, chosenAxisX), glm::dot(endPoint3a, chosenAxisY), 0);

	endPoint3a = endPoint3a - intPoint3a;
	target3a = target3a - intPoint3a;
	float theta = atan2(endPoint3a.y, endPoint3a.x) - atan2(target3a.y, target3a.x);
	//float theta = atan2(target3a.y, target3a.x) - atan2(endPoint3a.y, endPoint3a.x);
	float delta = _bvh->toDegrees(theta);
	if (delta <= 0.00001f && delta >= -0.00001f) {
		delta = 0;
	}
	return delta;
}